﻿using DiPractice1;

namespace Tests;

/// <summary>
/// Сервис для тестирования. всегда в качестве текущей даты возвращает ту, которая передана в конструкторе
/// </summary>
public class TestDateService : IDateService
{
    private DateTime _now;
    
    public TestDateService(DateTime now)
    {
        _now = now;
    }

    /// <inheritdoc />
    public DateTime Now => _now;
}