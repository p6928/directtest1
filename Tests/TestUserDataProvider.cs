﻿using DiPractice1;

namespace Tests;

/// <summary>
/// Тестовый сервис получения данных пользователя, возвращает то что передано в конструкторе
/// </summary>
public class TestUserDataProvider : IUserDataProvider
{
    private string? _userData;

    public TestUserDataProvider(string? userData)
    {
        _userData = userData;
    }

    /// <inheritdoc />
    public string? GetUserData()
    {
        return _userData;
    }
}