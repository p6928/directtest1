using DiPractice1;

namespace Tests;

[TestClass]
public class UserWorkServiceTests
{
    /// <summary>
    /// Успешная работа метода AddDate
    /// </summary>
    [TestMethod]
    public void AddDate_Success()
    {
        IDateService ds = new TestDateService(new DateTime(2023, 02, 24, 12, 23, 34));
        IUserDataProvider udp = new TestUserDataProvider("Люблю грозу в начале мая");
        
        UserWorkService uws = new UserWorkService(ds, udp, new UserDataValidateService(),null);

        var result = uws.AddDate("Люблю грозу в начале мая");
        
        Assert.AreEqual("Люблю грозу в начале мая 24.02.2023 12:23:34",result);
    }

    /// <summary>
    /// Успешная работа метода обработки
    /// </summary>
    [TestMethod]
    public void Work_Success()
    {
        IDateService ds = new TestDateService(new DateTime(2023, 02, 24, 12, 23, 34));
        IUserDataProvider udp = new TestUserDataProvider("Люблю грозу в начале мая");
        
        UserWorkService uws = new UserWorkService(ds, udp, new UserDataValidateService(),null);

        var result = uws.Work();
        
        Assert.IsTrue(result.IsValid);
        Assert.AreEqual("Люблю грозу в начале мая 24.02.2023 12:23:34",result.Payload);
    }
    
    /// <summary>
    /// Ошибка - пользователь предоставил null
    /// </summary>
    [TestMethod]
    public void Work_NullUserData()
    {
        IDateService ds = new TestDateService(new DateTime(2023, 02, 24, 12, 23, 34));
        IUserDataProvider udp = new TestUserDataProvider(null);
        
        UserWorkService uws = new UserWorkService(ds, udp, new UserDataValidateService(),null);

        var result = uws.Work();
        
        Assert.IsFalse(result.IsValid);
        Assert.AreEqual(ValidationResultCodes.NoData,result.ErrorCode);
    }
    
    
    /// <summary>
    /// Ошибка - пользователь ввел пробелы
    /// </summary>
    [TestMethod]
    public void Work_EmptyUserData()
    {
        IDateService ds = new TestDateService(new DateTime(2023, 02, 24, 12, 23, 34));
        IUserDataProvider udp = new TestUserDataProvider("       ");
        
        UserWorkService uws = new UserWorkService(ds, udp, new UserDataValidateService(),null);

        var result = uws.Work();
        
        Assert.IsFalse(result.IsValid);
        Assert.AreEqual(ValidationResultCodes.EmptyData,result.ErrorCode);
    }
    
    /// <summary>
    /// Ошибка - пользователь указал слишком много данных
    /// </summary>
    [TestMethod]
    public void Work_TooLongUserData()
    {
        IDateService ds = new TestDateService(new DateTime(2023, 02, 24, 12, 23, 34));
        IUserDataProvider udp = new TestUserDataProvider("   Это очень очень очено длинная предлинная строка, которая точно не пройдет валидацию    ");
        
        UserWorkService uws = new UserWorkService(ds, udp, new UserDataValidateService(),null);

        var result = uws.Work();
        
        Assert.IsFalse(result.IsValid);
        Assert.AreEqual(ValidationResultCodes.TooLong,result.ErrorCode);
    }
}