﻿namespace DiPractice1;

public class ConsoleUserDataProvider : IUserDataProvider
{
    /// <inheritdoc />
    public string? GetUserData()
    {
        Console.WriteLine();
        Console.Write("> ");
        return Console.ReadLine();
    }
}