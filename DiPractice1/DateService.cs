﻿namespace DiPractice1;

public class DateService : IDateService
{
    /// <inheritdoc />
    public DateTime Now => DateTime.Now;
}