﻿namespace DiPractice1;

public interface IDateService
{
    /// <summary>
    /// текущая дата 
    /// </summary>
    /// <returns></returns>
    DateTime Now { get; }
}