﻿using Microsoft.Extensions.Logging;

namespace DiPractice1;

/// <summary>
/// Сервис обработки пользовательского ввода
/// </summary>
public class UserWorkService
{
    /// <summary>
    /// Сервис дат
    /// </summary>
    private IDateService _ds;

    private readonly IUserDataProvider _userDataProvider;
    private readonly IUserDataValidateService _validationService;
    private readonly ILogger<UserWorkService> _logger;

    public UserWorkService(IDateService ds, IUserDataProvider userDataProvider, 
        IUserDataValidateService validationService, ILogger<UserWorkService> logger)
    {
        _ds = ds;
        _userDataProvider = userDataProvider;
        _validationService = validationService;
        _logger = logger;
    }

    /// <summary>
    /// Добавление текущей даты к строке
    /// </summary>
    /// <param name="input">входная строка</param>
    /// <returns></returns>
    public string AddDate(string? input)
    {
        return $"{input} {_ds.Now}";
    }

    /// <summary>
    /// Метод выполняет комплектную обработку
    /// </summary>
    /// <returns></returns>
    public ValidationResult<string> Work()
    {
        //Получаем пользовательские данные
        var data = _userDataProvider.GetUserData();
        _logger?.LogInformation($"Получены данные от пользователя: {data}");

        //проверяем данные
        var validateResult = _validationService.ValidateUserData(data);

        //если проверка не пройдена
        if (validateResult.IsValid == false)
        {
            _logger?.LogError($"Валидация данных не пройдена. ErrorCode: {validateResult.ErrorCode}; ErrorMessage: {validateResult.ErrorMessage}");
            return validateResult;
        }
        
        //обработка данных
        var workResult = AddDate(data);

        //Журналирование результата
        _logger?.LogInformation($"Данные обработаны. Результат: {workResult}");

        return new ValidationResult<string>(workResult);
    }
}