﻿namespace DiPractice1;

/// <summary>
/// Предопределенные коды валидации
/// </summary>
public static class ValidationResultCodes
{
    /// <summary>
    /// Нет ошибки
    /// </summary>
    public const int Valid = 0;

    /// <summary>
    /// Нет данных
    /// </summary>
    public const int NoData = 1;

    /// <summary>
    /// Пустые данные
    /// </summary>
    public const int EmptyData = 2;

    /// <summary>
    /// Слишком большой текст
    /// </summary>
    public const int TooLong = 10;

    /// <summary>
    /// Неопределенная, неожиданная ошибка
    /// </summary>
    public const int UnexpectedError = 1000;
}