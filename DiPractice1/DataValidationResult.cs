﻿namespace DiPractice1;

/// <summary>
/// Результат валидации данных пользователя
/// </summary>
public class ValidationResult<T>
{
    /// <summary>
    /// Результат успешен 
    /// </summary>
    public bool IsValid { get; set; }
    /// <summary>
    /// Код ошибки. 0 - нет ошибки
    /// </summary>
    public int ErrorCode { get; set; }
    /// <summary>
    /// Человекочитаемое сообщение об ошибке
    /// </summary>
    public string ErrorMessage { get; set; }

    /// <summary>
    /// Полезная нагрузка результата
    /// </summary>
    public T? Payload { get; set; }

    /// <summary>
    /// Создание результата валидации
    /// </summary>
    /// <param name="isValid">Результат верный</param>
    /// <param name="errorCode">код ошибки, 0 - нет ошибки</param>
    /// <param name="errorMessage">сообщение об ошибке</param>
    public ValidationResult(bool isValid = true, int errorCode = 0, string errorMessage = "", T? payload=default)
    {
        IsValid = isValid;
        ErrorCode = errorCode;
        ErrorMessage = errorMessage;
        Payload = payload;
    }

    /// <summary>
    /// Успешный результат с данными
    /// </summary>
    /// <param name="payload"></param>
    public ValidationResult(T payload)
    {
        IsValid = true;
        ErrorCode = 0;
        ErrorMessage = "";
        Payload = payload;
    }
}