﻿namespace DiPractice1;

/// <summary>
/// Сервис получения данных от пользователя
/// </summary>
public interface IUserDataProvider
{
    /// <summary>
    /// Получить данные пользователя
    /// </summary>
    /// <returns></returns>
    string? GetUserData();
}