﻿namespace DiPractice1;

/// <summary>
/// Конкретная валидация данных пользователей 
/// </summary>
public class UserDataValidateService : IUserDataValidateService
{
    /// <inheritdoc />
    public ValidationResult<string> ValidateUserData(string? data)
    {
        if (data is null)
            return new ValidationResult<string>()
            {
                ErrorCode = ValidationResultCodes.NoData,
                ErrorMessage = "Данные отсутствуют",
                IsValid = false,
                Payload = data
            };

        if (string.IsNullOrWhiteSpace(data))
            return new ValidationResult<string>(false, ValidationResultCodes.EmptyData, "Пустые данные",data);

        if (data.Length > 30)
            return new ValidationResult<string>(false, ValidationResultCodes.TooLong, "Слишком длинная строка",data);

        //Искусственная ошибка для тестирования 
        if (data == "error") throw new ArgumentException("Неверно указаны параметры");

        return new ValidationResult<string>(data);
    }
}