﻿//Задача:
//V - Консольное приложение на актуальной версии .Net,
//V - которое принимает в консоли строку
//V - и выводит ее же с текущими датой-временем,
//V - записывает в лог
//V - и правильно возвращает коды возврата,
//V - метод который добавляет дату к инпуту покрыт тестом,
//V - оформление и нейминг правильные.

using DiPractice1;
using Google.Apis.Requests;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Targets;

//Настраиваем DI
var serviceProvider = new ServiceCollection()
    .AddLogging()
    .AddSingleton<IDateService, DateService>()
    .AddSingleton<UserWorkService>()
    .AddSingleton<IUserDataProvider,ConsoleUserDataProvider>()
    .AddSingleton<IUserDataValidateService,UserDataValidateService>()
    .BuildServiceProvider();

//настройка журналирования
var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
loggerFactory.AddNLog();
loggerFactory.ConfigureNLog("nlog.config");

//выполняем всю логику
var worker = serviceProvider.GetRequiredService<UserWorkService>();
ValidationResult<string> result;
try
{
    result = worker.Work();
}
catch (Exception ex)
{
    Console.WriteLine($"Ошибка: {ex.GetType().FullName} {ex.Message}");
    Environment.Exit(ValidationResultCodes.UnexpectedError);
    return;
}

//вывод результата
if (result.IsValid)
{
    Console.WriteLine(result.Payload);
}
else
{
    Console.WriteLine($"Ошибка: {result.ErrorCode} {result.ErrorMessage}");
}

//выход с нужным кодом
Environment.Exit(result.ErrorCode);