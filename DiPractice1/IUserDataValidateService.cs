﻿namespace DiPractice1;

/// <summary>
/// Служба валидации пользовательских данных
/// </summary>
public interface IUserDataValidateService
{
    /// <summary>
    /// Проверить пользовательские данные
    /// </summary>
    /// <param name="data">пользовательские данные</param>
    /// <returns>результат валидации</returns>
    ValidationResult<string> ValidateUserData(string? data);
}